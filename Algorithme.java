package com.projetOS.metier;

import java.util.ArrayList;


/**
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */

public class Algorithme
{
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	protected ArrayList<Processus> File;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private int TpsDeService;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private int TauxPenallisation;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 */
	public Algorithme(){
		this.File = new ArrayList<Processus>();
	}
	
	public Algorithme(ArrayList<Processus> file){
		this.File = file;
	}

	/**
	 * @return the file
	 */
	public ArrayList<Processus> getFile() {
		return File;
	}

	/**
	 * @param file the file to set
	 */
	public void addProcessus(Processus processus) {
		File.add(processus);
	}

	/**
	 * @return the tauxPenallisation
	 */
	public int getTauxPenallisation() {
		return TauxPenallisation;
	}

	/**
	 * @param tauxPenallisation the tauxPenallisation to set
	 */
	public void setTauxPenallisation(int tauxPenallisation) {
		TauxPenallisation = tauxPenallisation;
	}

	/**
	 * @return the tpsDeService
	 */
	public int getTpsDeService() {
		return TpsDeService;
	}

	/**
	 * @param tpsDeService the tpsDeService to set
	 */
	public void setTpsDeService(int tpsDeService) {
		TpsDeService = tpsDeService;
	}

}

