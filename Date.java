package com.projetOS.metier;

public class Date {
	private double temps;
	private int PID;
	private String action;
	
	/**
	 * @param temps 
	 * @param PID 
	 * @param action 
	 * 
	 */
	public Date(double temps, int PID, String action){
		this.setTemps(temps);
		this.setPID(PID);
		this.setAction(action);
	}

	public double getTemps() {
		return temps;
	}

	public void setTemps(double temps) {
		this.temps = temps;
	}

	public int getPID() {
		return PID;
	}

	public void setPID(int pID) {
		PID = pID;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

}
